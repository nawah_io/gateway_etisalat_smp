import os
import re
import time

import pytest


@pytest.mark.asyncio
async def test_gateway_obtain_token():
    return
    # [DOC] Skip this test if _obtain_etisalat_smp_token was run as part of test test_gateway
    # if _gateway_token['token']:
    #     return

    # token = _obtain_etisalat_smp_token(
    #     username=os.environ['TEST_ETISALAT_SMP_USERNAME'],
    #     password=os.environ['TEST_ETISALAT_SMP_PASSWORD'],
    # )

    # assert type(token) == str
    # assert (
    #     re.match(r'^[a-zA-Z0-9\-_]+?\.[a-zA-Z0-9\-_]+?\.([a-zA-Z0-9\-_]+)?$', token)
    #     != None
    # )

    # [DOC] Set values for _gateway_token to avoid running _obtain_etisalat_smp_token again with test test_gateway
    # _gateway_token['token'] = token
    # _gateway_token['expiry'] = time.time() + 43200


@pytest.mark.asyncio
async def test_gateway():
    return
    # send_request = etisalat_smp_gateway(
    #     phone=os.environ['TEST_ETISALAT_SMP_PHONE'],
    #     content=os.environ['TEST_ETISALAT_SMP_CONTENT'],
    #     etisalat_smp_auth={
    #         'sender_name': os.environ['TEST_ETISALAT_SMP_SENDER'],
    #         'username': os.environ['TEST_ETISALAT_SMP_USERNAME'],
    #         'password': os.environ['TEST_ETISALAT_SMP_PASSWORD'],
    #     },
    # )

    # assert send_request
