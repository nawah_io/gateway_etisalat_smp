import datetime
import logging
import os
import time
from typing import TYPE_CHECKING, TypedDict

import requests
from nawah.classes import Attr, Func, Module, Package, Perm, Var
from nawah.config import Config
from nawah.utils import var_value

if TYPE_CHECKING:
    from nawah.types import NawahDoc, Results

logger = logging.getLogger('nawah')


__version__ = '0.0.0'

with open(
    os.path.join(os.path.dirname(__file__), 'version.txt'), encoding='UTF-8'
) as f:
    __version__ = f.read().strip()

GATEWAY_ETISALAT_SMP_TOKEN_DICT = TypedDict(
    'GATEWAY_ETISALAT_SMP_TOKEN_DICT', {'token': str, 'expiry': float}
)

gateway_token: GATEWAY_ETISALAT_SMP_TOKEN_DICT = {
    'token': '',
    'expiry': 0,
}


async def _send_sms(doc: 'NawahDoc') -> 'Results':
    phone = doc['phone'].replace('+', '')
    content = doc['content']
    msg_category = doc['msg_category']
    campaign_desc = doc['campaign_desc'] if 'campaign_desc' in doc else None
    campaign_name = doc['campaign_name'] if 'campaign_name' in doc else None
    content_type = doc['content_type'] if 'content_type' in doc else None
    expiry_date = doc['expiry_date'] if 'expiry_date' in doc else None
    dnd_category = doc['dnd_category'] if 'dnd_category' in doc else None
    client_id = doc['client_id'] if 'client_id' in doc else None
    etisalat_smp_auth = doc['etisalat_smp_auth'] if 'etisalat_smp_auth' in doc else None

    global gateway_token

    if not etisalat_smp_auth:
        etisalat_smp_auth = var_value(Var.CONFIG('etisalat_smp'))

    if not expiry_date:
        expiry_date = (
            datetime.datetime.utcnow() + datetime.timedelta(minutes=15)
        ).isoformat().split('.')[0] + 'Z'

    if Config.sys.cache:
        try:
            _gateway_token = await Config.sys.cache.get('__etisalat_smp')
            if not _gateway_token:
                _gateway_token = {
                    'token': '',
                    'expiry': 0,
                }
            gateway_token = _gateway_token
        except:
            pass

    if not gateway_token['token'] or time.time() > gateway_token['expiry']:
        gateway_token['token'] = _obtain_etisalat_smp_token(
            username=etisalat_smp_auth['username'],
            password=etisalat_smp_auth['password'],
        )
        gateway_token['expiry'] = time.time() + 21350

        if Config.sys.cache:
            try:
                await Config.sys.cache.set('__etisalat_smp', '.', gateway_token)
            except:
                pass

    endpoint = 'https://smartmessaging.etisalat.ae:5676/campaigns/submissions/sms/nb'

    request_json = {
        'campaignName': campaign_name,
        'desc': campaign_desc,
        'msgCategory': msg_category,
        'contentType': content_type,
        'senderAddr': etisalat_smp_auth['sender_name'],
        'expiryDt': expiry_date,
        'dndCategory': dnd_category,
        'priority': 1,
        'clientTxnId': client_id,
        'recipient': phone,
        'dr': '1',
        'msg': content,
    }

    send_request = requests.post(
        endpoint,
        json=request_json,
        headers={'Authorization': 'bearer ' + gateway_token['token']},
    )

    if send_request.status_code != 201:
        logger.error(
            'Failed to send message using \'etisalat_smp\', with following body:'
        )
        logger.error(request_json)
        logger.error(send_request.text)
        raise Exception('Failed to send message')

    return send_request.json()


def _obtain_etisalat_smp_token(username: str, password: str):
    token_request = requests.post(
        'https://smartmessaging.etisalat.ae:5676/login/user',
        json={
            'username': username,
            'password': password,
        },
    )

    if token_request.status_code != 200:
        raise Exception('Failed to authinticate to Etisalat SMP')

    return token_request.json()['token']


etisalat_smp = Module(
    name='etisalat_smp',
    funcs={
        'send_sms': Func(
            permissions=[Perm(privilege='__sys')],
            doc_attrs={
                'phone': Attr.PHONE(),
                'content': Attr.STR(),
                'msg_category': Attr.STR(),
            },
            callable=_send_sms,
        )
    },
)

nawah_etisalat_smp = Package(
    name='nawah_etisalat_smp',
    api_level='2.0',
    version=__version__,
    vars_types={
        'etisalat_smp': Attr.TYPED_DICT(
            dict={
                'sender_name': Attr.STR(),
                'username': Attr.STR(),
                'password': Attr.STR(),
            }
        ),
    },
    modules=[etisalat_smp],
)
