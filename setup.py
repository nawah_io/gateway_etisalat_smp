import os.path

import setuptools


def _file_abs_path(*path: str) -> str:
    return os.path.join(os.path.dirname(__file__), *path)


with open(_file_abs_path('nawah_etisalat_smp', 'version.txt'), encoding='UTF-8') as f:
    __version__ = f.read().strip()

with open(_file_abs_path('README.md'), 'r', encoding='UTF-8') as f:
    long_description = f.read()

with open(_file_abs_path('requirements.txt'), 'r', encoding='UTF-8') as f:
    requirements = f.readlines()

with open(_file_abs_path('dev_requirements.txt'), 'r', encoding='UTF-8') as f:
    dev_requirements = f.readlines()

setuptools.setup(
    name='nawah_etisalat_smp',
    version=__version__,
    author='Mahmoud Abduljawad',
    author_email='mahmoud@masaar.com',
    description='Nawah package for Etisalat SMS',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/nawah_io/nawah_etisalat_smp',
    package_data={
        'nawah_etisalat_smp': ['py.typed', 'version.txt'],
    },
    packages=[
        'nawah_etisalat_smp',
    ],
    project_urls={
        'Docs: Gitlab': 'https://gitlab.com/nawah_io/nawah_etisalat_smp',
        'Gitlab: issues': 'https://gitlab.com/nawah_io/nawah_etisalat_smp/-/issues',
        'Gitlab: repo': 'https://gitlab.com/nawah_io/nawah_etisalat_smp',
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Topic :: Internet :: WWW/HTTP',
        'Framework :: AsyncIO',
    ],
    python_requires='>=3.10.2',
    install_requires=requirements,
    extras_require={'dev': dev_requirements},
)
