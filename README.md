# Nawah Gateway for Etisalat SMP

This repo is a [Nawah](https://github.com/nawah-io/nawah_docs) Package that allows developers to integrate [Etisalat SMP](https://www.etisalat.ae/en/smb/digital-solutions/smart-messaging.jsp) into Nawah apps using Gateway Controller.

## How-to

1. From your app directory run: `nawah packages add gateway_etisalat`
2. Add `etisalat_smp` Var to `nawah_app.py` App Config:

```python
vars = {
	'etisalat_smp': {
      'sender_name': 'ETISALAT_SMP_SENDER',
      'username': 'ETISALAT_SMP_USERNAME',
      'password':'ETISALAT_SMP_PASSWORD',
   }
}
```

3. `etisalat_smp` gateway requires following args:
   1. `phone`: Target phone number using international format with prefixed `+`. Type `str`.
   2. `content`: Message body. Type `str`.
4. `etisalat_smp` gateway accepts following optionsal args
   1. `etisalat_smp_auth`, replicating `etisalat_smp` value in `vars Config Attr` for dynamic Etisalat SMP credentials.
   2. `campaign_desc`: Sets value for `desc` attribute for Etisalat SMP non-bulk message request.
   3. `campaign_name`: Sets value for `campaignName` attribute for Etisalat SMP non-bulk message request.
   4. `msg_category`: Sets value for `msgCategory` for Etisalat SMP non-bulk message request.
   5. `content_type`: Sets value for `contentType` for Etisalat SMP non-bulk message request.
   6. `expiry_date`: Sets value for `expiryDt` for Etisalat SMP non-bulk message request.
   7. `dnd_category`: Sets value for `dndCategory` for Etisalat SMP non-bulk message request.
   8. `client_id`: Sets value for `clientTxnId` for Etisalat SMP non-bulk message request.
5. Use `etisalat_smp` gateway using Nawah Gateway Controller:

```python
from nawah.gateway import Gateway

Gateway.send(gateway='etisalat_smp', phone=phone, content=content)
```
